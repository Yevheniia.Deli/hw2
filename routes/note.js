const {Router: router} = require('express');
const {getAll, getOne, create, check, del} = require('../controllers/note');
const noteRouter = router();

noteRouter.get('/', getAll);
noteRouter.get('/:id', getOne);
noteRouter.post('/', create);
noteRouter.patch('/:id', check);
noteRouter.delete('/:id', del);

module.exports = noteRouter;
