const {Router: router} = require('express');
const {getOne} = require('../controllers/user');
const userRouter = router();

userRouter.get('/me', getOne);

module.exports = userRouter;
