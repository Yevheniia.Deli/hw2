const mongoose = require('mongoose');

const NoteSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    required: true,
    default: Date.now,
  },
});
const Note = mongoose.model('Note', NoteSchema);

module.exports = Note;
