const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();


exports.register = async function(req, res, next) {
  const {username, password} = req.body;
  if (!username || !password) {
    return res.status(400).send({message: 'Username or password is missing'});
  }
  try {
    const hash = await bcrypt.hash(password, 1);

    const newUser = new User({username, password: hash});
    await newUser.save();
    res.send({
      message: 'Success',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.login = async function(req, res, next) {
  const {username, password} = req.body;
  if (!username || !password) {
    return res.status(400).send({message: 'Username or password not found'});
  }
  const user = await User.findOne({username});
  if (!user) {
    return res.status(400).send({message: 'User not found'});
  }
  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    return res.status(400).send({message: 'Incorrect password'});
  }
  return res.status(200).send({
    message: 'Success',
    jwt_token: jwt.sign({id: user._id,
      username: user.username}, process.env.TOKEN_KEY),
  });
};
