const User = require('../models/user');

exports.getOne = async function(req, res, next) {
  try {
    const user = await User.findOne({_id: req.userId});
    if (!user) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({user: {username: user.username,
      _id: user._id, createdDate: user.createdDate}});
  } catch (error) {
    res.status(500).send(error);
  }
};
