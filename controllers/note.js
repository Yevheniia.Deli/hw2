const Note = require('../models/note');

exports.getAll = async function(req, res, next) {
  const {offset, limit} = req.query;
  const notes = await Note.find({userId: req.userId});
  try {
    const notesData = notes.splice(offset || 0, limit || notes.length);
    res.send({
      offset: Number(offset) || 0,
      limit: Number(limit) || 0,
      count: notesData.length,
      notes: notesData,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getOne = async function(req, res, next) {
  const {id} = req.params;
  try {
    const note = await Note.findOne({_id: id, userId: req.userId});
    if (!note) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send(note);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.create = async function(req, res, next) {
  if (!req.body.text) {
    return res.status(400).send({message: 'Text is missing'});
  }
  const newNote = new Note({...req.body, userId: req.userId});
  try {
    await newNote.save();
    res.send({
      message: 'Success',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.check = async function(req, res, next) {
  const {id} = req.params;
  try {
    const note = await Note.findOne({_id: id, userId: req.userId});
    if (!note) {
      return res.status(400).send({message: 'Not found'});
    }
    await Note.updateOne({_id: id,
      userId: req.userId}, {completed: !note.completed});
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.del = async function(req, res, next) {
  const {id} = req.params;
  try {
    const deleteInfo = await Note.deleteOne({_id: id, userId: req.userId});
    if (deleteInfo.deletedCount < 1) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send(error);
  }
};
